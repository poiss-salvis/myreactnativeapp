import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface TaskRecordType {
    name: string,
    isDone: boolean,
}

export interface TasksState {
    tasks: Array<TaskRecordType>,
}

type TasksStateType = TasksState | null | undefined;
// for dev. purposes
const initialState: TasksStateType = {
    tasks: new Array(),
};

const tasksSlice = createSlice({
    name: 'tasks',
    initialState,
    reducers: {
        addTask(state: TasksState, action: PayloadAction<string>) {
            state.tasks.push(
                {
                    name: action.payload,
                    isDone: false,
                });
        },
        removeTask(state: TasksState, action: PayloadAction<number>) {
            state.tasks.splice(action.payload, 1); // remove 1 element starting from index N
        },
        changeTaskState(state: TasksState, action: PayloadAction<{ index: number, isDone: boolean }>) {
            state.tasks[action.payload.index].isDone = action.payload.isDone;
        },
    }
})

// export reducer actions
export const { addTask, removeTask, changeTaskState } = tasksSlice.actions;
// export so we can combine multiple
export default tasksSlice.reducer;