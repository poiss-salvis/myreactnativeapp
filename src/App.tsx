import React from 'react';

// navigation
import { NavigationContainer, RouteProp } from '@react-navigation/native';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';

// Redux
import { Provider } from 'react-redux'
import configureStore from './Store/configureStore';

// Screens
import HomeScreen from './Screens/Home';
import SecondaryScreen from './Screens/Secondary';
import UIStoreScreen from './Screens/UIStore';
import TasksScreen from './Screens/Tasks';

// export common props type for screen components
export type NavigationProps<RouteName extends keyof RootStackParamList> = {
  navigation: StackNavigationProp<RootStackParamList, RouteName>;
  route: RouteProp<RootStackParamList, RouteName>;
};

// register pages and prop types
// props can be `undefined` as we will use Redux to pass data between screens
// this will just force us to use strictly-typed names
type RootStackParamList = {
  Home: undefined;
  Secondary: { userId: string } | undefined;
  UIStore: undefined;
  Tasks: undefined;
};

// configure app navigator
const RootStack = createStackNavigator<RootStackParamList>();

// configure redux store
const store = configureStore({
  UI: {
    processing: true, // set initial state value instead of one defined as default
  }
}); // create new Redux store

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootStack.Navigator initialRouteName="Home">
          <RootStack.Screen name="Home" component={HomeScreen} />
          <RootStack.Screen name="Secondary" component={SecondaryScreen} />
          <RootStack.Screen name="UIStore" component={UIStoreScreen} />
          <RootStack.Screen name="Tasks" component={TasksScreen} />
        </RootStack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}