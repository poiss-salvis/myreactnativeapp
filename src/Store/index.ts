import * as UI from './states/UI';
import * as Tasks from './states/Tasks';
import { useSelector, TypedUseSelectorHook } from 'react-redux'

export const useTypedSelector: TypedUseSelectorHook<ApplicationState> = useSelector;

export interface ApplicationState {
    UI: UI.UiState | undefined,
    Tasks: Tasks.TasksState | undefined,
}

export const reducers = {
    UI: UI.default,
    Tasks: Tasks.default,
}