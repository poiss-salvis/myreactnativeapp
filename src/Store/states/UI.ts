import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface UiState {
    processing: boolean,
}

type UiStateType = UiState | null | undefined;
// for dev. purposes
const initialState: UiStateType = {
    processing: false,
};

const uiSlice = createSlice({
    name: 'ui',
    initialState,
    reducers: {
        setProcessing(state: UiState, action: PayloadAction<boolean>) {
            state.processing = action.payload;
        }
    }
})

// export reducer actions
export const { setProcessing } = uiSlice.actions;
// export so we can combine multiple
export default uiSlice.reducer;