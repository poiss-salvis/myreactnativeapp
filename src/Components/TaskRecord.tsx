import React from 'react';
import { Text, StyleSheet, TextProps } from 'react-native';

import { TaskRecordType } from '../Store/states/Tasks';

type Props = TaskRecordType & TextProps;

const styles = StyleSheet.create({
    done: {
        textDecorationLine: 'line-through',
    },
});

const TaskRecord = (props: Props) => {
    return (
        <Text {...props} style={[props.style, props.isDone && styles.done]}>{props.name}</Text>
    );
}

export default TaskRecord;