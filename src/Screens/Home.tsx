import React from 'react';
import { Text, View, Button } from 'react-native';
import GlobalStyles from './../Styles';

// navigation
import { NavigationProps } from '../App';

const HomeScreen = ({ navigation }: NavigationProps<'Home'>) => {
  const currentNavState = navigation.dangerouslyGetState();
  const routes = currentNavState.routeNames;
  //const currentRoute = currentNavState.routes[currentNavState.index];

  return (
    <View style={GlobalStyles.container}>
      <Text>This is 'src/Screens/Home.tsx' file</Text>
      {routes.map(key => {
        if (key == 'Home') {
          return;
        }

        return (
          <Button
            key={key}
            title={`Go to ${key}`}
            onPress={() => {
              let routeParams: any;
              if (key == 'Secondary') {
                routeParams = { userId: 'Test' };
              }

              navigation.navigate(key, routeParams);
            }}
          />
        )
      })}
    </View>
  );
}

export default HomeScreen;