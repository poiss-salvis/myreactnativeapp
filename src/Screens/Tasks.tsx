import React from 'react';
import { SafeAreaView, View, Button, FlatList, TextInput, StyleSheet } from 'react-native';
import GlobalStyles, { GlobalStyleVariables } from './../Styles';

// navigation
import { NavigationProps } from '../App';

// Redux
import { useDispatch } from "react-redux";
import { addTask, changeTaskState, removeTask } from '../Store/states/Tasks';
import { useTypedSelector } from '../Store';

import TaskRecord from '../Components/TaskRecord';

const styles = StyleSheet.create({
  input: {
    fontSize: 20,
  },
  spacing: {
    padding: GlobalStyleVariables.Gutter,
  },
  bottomSeparatorLine: {
    borderBottomWidth: 1,
    borderBottomColor: '#ebedf0',
  },
  fullWidth: {
    width: '100%',
  },
  taskRecord: {
    paddingBottom: GlobalStyleVariables.Gutter / 2,
  }
});

const TasksScreen = ({ navigation }: NavigationProps<'Tasks'>) => {
  const tasksState = useTypedSelector(state => state.Tasks); // monitor state changes
  const dispatch = useDispatch(); // used to call reducer actions
  const [inputValue, onInputChangeText] = React.useState<string>('');

  const addTaskHandler = () => {
    if (inputValue) {
      dispatch(addTask(inputValue));
    }
    onInputChangeText('');
  }

  const changeTaskStateHandler = (index: number, isDone: boolean) => {
    console.log(`changing ${index} state to ${isDone}`);
    dispatch(changeTaskState({ index, isDone }));
  }

  return (
    <SafeAreaView style={GlobalStyles.container}>
      {/* you can assign multiple styles as array */}
      <View style={[styles.spacing, styles.fullWidth, styles.bottomSeparatorLine]}>
        <TextInput
          style={styles.input}
          placeholder="Some to do task"
          value={inputValue}
          onChangeText={text => onInputChangeText(text)}
        />
        <Button
          disabled={!inputValue}
          title="Add"
          onPress={() => addTaskHandler()}
        />
      </View>
      <FlatList
        style={styles.spacing}
        data={tasksState?.tasks}
        renderItem={elem =>
          <TaskRecord
            style={styles.taskRecord}
            {...elem.item}
            onPress={() => changeTaskStateHandler(elem.index, !elem.item.isDone)}
            onLongPress={() => dispatch(removeTask(elem.index))}
          />
        }
        keyExtractor={(elem, index) => index.toString()}
      />
    </SafeAreaView >
  );
}

export default TasksScreen;