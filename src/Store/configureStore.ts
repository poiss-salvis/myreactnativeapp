import { applyMiddleware, combineReducers, createStore, AnyAction } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from "redux-devtools-extension";

import { ApplicationState, reducers } from './';

export default function configureStore(initialState?: ApplicationState) {
    const rootReducer = combineReducers({
        ...reducers,
    });

    const middlewares = [thunkMiddleware];
    const enhancers: any[] = [];

    // used instead of compose() to allow easier store debugging @ browsers Redux DevTools extension
    const composedEnhancers = composeWithDevTools(applyMiddleware(...middlewares), ...enhancers);

    const store = createStore(
        rootReducer,
        initialState,
        composedEnhancers);

    return store;
}