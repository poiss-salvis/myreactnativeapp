import { StyleSheet } from 'react-native';

export const GlobalStyleVariables = {
    Gutter: 16,
}

export const GlobalStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default GlobalStyles;