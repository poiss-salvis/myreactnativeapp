import React from 'react';
import { Text, View, Button, Switch } from 'react-native';
import GlobalStyles from './../Styles';

// navigation
import { NavigationProps } from '../App';

// Redux
import { useDispatch } from "react-redux";
import { setProcessing } from '../Store/states/UI';
import { useTypedSelector } from '../Store';

const UIStoreScreen = ({ navigation }: NavigationProps<'UIStore'>) => {
  const uiState = useTypedSelector(state => state.UI); // monitor state changes
  const dispatch = useDispatch(); // used to call reducer actions

  return (
    <View style={GlobalStyles.container}>
      <Text>This is 'src/Screens/UIStore.tsx' file</Text>
      <Text>IsProcessing? {uiState.processing.toString()}
        <Switch
          onValueChange={value => dispatch(setProcessing(value))}
          value={uiState.processing}
        />
      </Text>
    </View>
  );
}

export default UIStoreScreen;