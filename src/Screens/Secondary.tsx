import React from 'react';
import { Text, View, Button } from 'react-native';
import GlobalStyles from './../Styles';

// navigation
import { NavigationProps } from '../App';

const SecondaryScreen = ({ navigation, route }: NavigationProps<'Secondary'>) => {
  return (
    <View style={GlobalStyles.container}>
      <Text>This is 'src/Screens/Secondary.tsx' file</Text>
      {route?.params?.userId && <Text>Passed UserID: {route.params.userId}</Text>}
    </View>
  );
}

export default SecondaryScreen;